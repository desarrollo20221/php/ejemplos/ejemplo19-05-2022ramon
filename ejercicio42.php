<?php
    require './libreria.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?= css(); ?>
        <title>Hello, world!</title>
    </head>
    <body>
        <?php
            require './menu.php';
        ?>
        
        <?= galeria(1); ?>

       <?= js(); ?>
    </body>
</html>