<?php

function css() {
    ?>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
          crossorigin="anonymous">
          <?php
      }

      function js() {
          ?>            
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous">
    </script>
    <?php
}

function resultados($ejercicio) {
    switch ($ejercicio) {
        case "ejercicio1":
            require 'resultados1.php';
            break;
        case "ejercicio2":
            require 'resultados2.php';
            break;
        case "ejercicio3":
            require 'resultados3.php';
            break;
        case "ejercicio3s":
            $numeros = $_GET["numeros"];
            $suma = 0;
            $resto = explode(";", $numeros[2]);
            unset($numeros[2]);
            $numeros = array_merge($numeros, $resto);
            foreach ($numeros as $numero) {
                $suma += $numero;
            }
            require 'resultados3s.php';
            break;
        case "ejercicio3p":
            $numeros = $_GET["numeros"];
            $pro = 1;
            $resto = explode(";", $numeros[2]);
            unset($numeros[2]);
            $numeros = array_merge($numeros, $resto);
            foreach ($numeros as $numero) {
                $pro *= $numero;
            }
            require 'resultados3p.php';
            break;
    }
}

/**
 * 
 * @param string $ejercicio formulario que quiero que muestre
 * @param bool $validar si quiero que realize la validacion de datos
 */
function formularios($ejercicio, $validar = true) {
    switch ($ejercicio) {
        case "ejercicio1":
            require 'formulario1.php';
            break;
        case "ejercicio2":
            require 'formulario2.php';
            break;
        case "ejercicio3":
            require 'formulario3.php';
            break;
        case "ejercicio3s":
            require 'formulario3s.php';
            break;
        case "ejercicio3p":
            require 'formulario3p.php';
            break;
    }
}

function galeria($numero) {
    $galerias=[
        [
            [
                "titulo" => "Foto 1",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f1.jpg",
                "fecha" => "12 Enero 2022",
            ],
            [
                "titulo" => "Foto 2",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f2.jpg",
                "fecha" => "12 Enero 2022",
            ],
            [
                "titulo" => "Foto 3",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f3.jpg",
                "fecha" => "12 Enero 2022",
            ],
            [
                "titulo" => "Foto 4",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f4.jpg",
                "fecha" => "12 Enero 2022",
            ],
        ],
        [
            [
                "titulo" => "Foto 5",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f5.jpg",
                "fecha" => "12 Enero 2022",
            ],
            [
                "titulo" => "Foto 6",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f6.jpg",
                "fecha" => "12 Enero 2022",
            ],
            [
                "titulo" => "Foto 7",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f7.jpg",
                "fecha" => "12 Enero 2022",
            ],
            [
                "titulo" => "Foto 8",
                "texto" => "Lorem ipsum",
                "src" => "./imgs/f8.jpg",
                "fecha" => "12 Enero 2022",
            ],
        ]
    ];
    
    require 'galerias.php';
}
?>



