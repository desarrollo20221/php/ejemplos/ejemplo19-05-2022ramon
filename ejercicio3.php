<?php
require_once './libreria.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?= css(); ?>
        <title>Hello, world!</title>
    </head>
    <body>
        <?php
            require './menu.php';
        ?>
        <div class="my-4 container-fluid">
            <?php
            
            if(isset($_GET["ejercicio3s"])){
                // he apretado el boton de sumar
                resultados("ejercicio3s");
                
            }elseif(isset($_GET["ejercicio3p"])){
                // he pulsado el boton de multiplicar
                resultados("ejercicio3p");
                
            }elseif(isset($_GET["ejercicio3"])){
                // he pulsado el boton de todo
                resultados("ejercicio3s");
                resultados("ejercicio3p");
                
            }else{
                // cargar el formulario
                formularios("ejercicio3");
            }

            ?>
            </div>
            <?= js(); ?>            
    </body>
</html>