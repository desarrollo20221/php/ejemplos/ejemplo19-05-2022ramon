<?php
require_once './libreria.php';
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <?= css(); ?>
        <title>Hello, world!</title>
    </head>
    <body>
        <?php
        require './menu.php';
        ?>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio1"])) {
                
                 resultados("ejercicio1");
                
            } else {
                
                formularios("ejercicio1");
                
            }
            ?>
        </div>
        <?= js(); ?>
    </body>
</html>